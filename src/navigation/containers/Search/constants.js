export const CHANGE_SEARCH_INPUT = 'hh/containers/search/CHANGE_SEARCH_INPUT';

export const STAFF_SEARCH_REQUEST = 'hh/containers/search/STAFF_SEARCH_REQUEST';
export const STAFF_SEARCH_REQUEST_SUCCESS = 'hh/containers/search/STAFF_SEARCH_REQUEST_SUCCESS';
export const STAFF_SEARCH_REQUEST_FAIL = 'hh/containers/search/STAFF_SEARCH_REQUEST_FAIL';

export const PROFILE_SEARCH_REQUEST = 'hh/containers/search/PROFILE_SEARCH_REQUEST';
export const PROFILE_SEARCH_REQUEST_SUCCESS = 'hh/containers/search/PROFILE_SEARCH_REQUEST_SUCCESS';
export const PROFILE_SEARCH_REQUEST_FAIL = 'hh/containers/search/PROFILE_SEARCH_REQUEST_FAIL';
