import styled from 'styled-components';

const Flex = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  margin-top: 63px;
`;

export default Flex;
