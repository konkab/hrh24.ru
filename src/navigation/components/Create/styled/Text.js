import styled from 'styled-components';

const Text = styled.p`
  margin-top: 0;
  margin-left: 12px;
  font-weight: 600;
`;

export default Text;
