import React from 'react';
import { Link, Route } from 'react-router-dom';

const LinkItem = ({ components: C, to, ...props }) => (
  <Route
    render={({ location: { pathname } }) => (
      <C.Item
        component={({ children, className }) => (
          <Link className={className} to={to}>
            {children}
          </Link>
        )}
        isSelected={pathname === to}
        {...props}
      />
    )}
  />
);

export default LinkItem;
