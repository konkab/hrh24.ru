import React from 'react';
import { DropdownItemGroup, DropdownItem } from '@atlaskit/dropdown-menu';

const HelpDropdown = () => (
  <div>
    <DropdownItemGroup title="Поддержка">
      <DropdownItem
        href="https://api.whatsapp.com/send/?phone=79830007000"
        target="_blank"
        rel="noopener noreferrer"
      >
        Связаться в WhatsApp
      </DropdownItem>
      <DropdownItem href="https://t.me/eg0rgaiduk" target="_blank" rel="noopener noreferrer">
        Связаться в Telegram
      </DropdownItem>
    </DropdownItemGroup>
    <DropdownItemGroup title="Правовая информация">
      <DropdownItem href="/documents/2" rel="noopener noreferrer" target="_blank">
        Правила оплаты
      </DropdownItem>
      <DropdownItem href="/documents/1" rel="noopener noreferrer" target="_blank">
        Договор-оферта
      </DropdownItem>
      <DropdownItem href="/documents/3" rel="noopener noreferrer" target="_blank">
        Политика конфиденциальности
      </DropdownItem>
      <DropdownItem href="/documents/4" rel="noopener noreferrer" target="_blank">
        Пользовательское соглашение
      </DropdownItem>
    </DropdownItemGroup>
  </div>
);

export default HelpDropdown;
