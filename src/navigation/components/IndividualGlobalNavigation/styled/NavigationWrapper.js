import styled from 'styled-components';

const NavigationWrapper = styled.div`
  @media print {
    display: none;
  }
`;

export default NavigationWrapper;
