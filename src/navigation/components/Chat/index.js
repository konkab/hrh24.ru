import React from 'react';

class Chat extends React.Component {
  state = {
    isOpened: false
  };

  onClick = () => {
    console.log(window.jivo_api);
  };

  render() {
    return <div className="react-component-chat" onClick={this.onClick} />;
  }
}

export default Chat;
