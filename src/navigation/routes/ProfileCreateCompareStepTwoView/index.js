import React, { Fragment } from 'react';
import RootViews from 'navigation/routes/RootViews';
import SetActiveView from 'navigation/routes/SetActiveView';
import ProfileCreateCompareStepTwoPage from 'pages/ProfileCreateCompare/StepTwo';

const ProfileCreateCompareStepTwoView = () => (
  <Fragment>
    <RootViews />
    <SetActiveView id="root/index" />
    <ProfileCreateCompareStepTwoPage />
  </Fragment>
);

export default ProfileCreateCompareStepTwoView;
