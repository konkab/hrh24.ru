import React, { Fragment } from 'react';
import ProfileRootView from 'navigation/routes/Partner/RootView';
import SetActiveView from 'navigation/routes/SetActiveView';

import PartnerListPage from 'pages/PartnerProfile/Partners/List';

const PartnerListView = () => (
  <Fragment>
    <ProfileRootView />
    <SetActiveView id="partner/index" />
    <PartnerListPage />
  </Fragment>
);

export default PartnerListView;
