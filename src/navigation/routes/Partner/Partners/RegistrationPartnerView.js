import React, { Fragment } from 'react';
import ProfileRootView from 'navigation/routes/Partner/RootView';
import SetActiveView from 'navigation/routes/SetActiveView';

import RegistrationPage from 'pages/PartnerProfile/Partners/Registration';

const RegistrationPartnerView = () => (
  <Fragment>
    <ProfileRootView />
    <SetActiveView id="partner/index" />
    <RegistrationPage />
  </Fragment>
);

export default RegistrationPartnerView;
