import { Component } from 'react';
import { withNavigationViewController } from '@atlaskit/navigation-next';

class SetActiveViewBase extends Component {
  componentDidMount() {
    const { id, navigationViewController } = this.props;
    const { containerViewId, productViewId } = navigationViewController.state;
    if (id !== containerViewId && id !== productViewId) {
      navigationViewController.setView(id);
    }
  }

  render() {
    return null;
  }
}

const SetActiveView = withNavigationViewController(SetActiveViewBase);

export default SetActiveView;
