import React, { Fragment } from 'react';
import RootViews from 'navigation/routes/RootViews';
import SetActiveView from 'navigation/routes/SetActiveView';
import ProfileCreateAskStepThreePage from 'pages/ProfileCreateAsk/StepThree';

const ProfileCreateAskStepThreeView = () => (
  <Fragment>
    <RootViews />
    <SetActiveView id="root/index" />
    <ProfileCreateAskStepThreePage />
  </Fragment>
);

export default ProfileCreateAskStepThreeView;
