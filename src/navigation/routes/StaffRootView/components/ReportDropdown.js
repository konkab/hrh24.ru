import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import { Item } from '@atlaskit/navigation-next';
import { DropdownItem, DropdownItemGroup } from '@atlaskit/dropdown-menu';
import ChevronUpIcon from '@atlaskit/icon/glyph/chevron-up';
import ChevronDownIcon from '@atlaskit/icon/glyph/chevron-down';
import QueuesIcon from '@atlaskit/icon/glyph/queues';

import { changeReportShape } from 'pages/IndividualProfile/Reports/actions';

import BetterDropdown from "../../../containers/ReportDropdown/BetterDropdown";
import injectReducer from "../../../../utils/injectReducer";
import reducer from "pages/IndividualProfile/Reports/reducer";

const ReportDropdown = ({ changeReportShape, reportShape = 0, ...props }) => (
  <BetterDropdown
    trigger={({ isOpen = false }) => (
      <Item
        before={() => <QueuesIcon />}
        isSelected={isOpen || props.to === props.match.url}
        text={props.reportShapeText[reportShape].label}
        after={() => (isOpen ? <ChevronUpIcon /> : <ChevronDownIcon />)}
      />
    )}
  >
    <DropdownItemGroup>
      {props.reportShapeText.map(dropdown => (
        <DropdownItem
          key={dropdown.id}
          isSelected={dropdown.id === reportShape}
          onClick={() => {
            if (props.to !== props.match.url) {
              props.history.push(props.to);
            }

            changeReportShape({ reportShape: dropdown.id });
          }}
        >
          {dropdown.label}
        </DropdownItem>
      ))}
    </DropdownItemGroup>
  </BetterDropdown>
);

function mapDispatchToProps(dispatch) {
  return {
    changeReportShape: value => dispatch(changeReportShape(value))
  };
}

const mapStateToProps = store => ({
  reportShape: store.getIn(['individualReport', 'reportShape']),
  reportShapeText: (() => {
    const report = store.getIn(['individualReport', 'report']);
    const reportShapeText = [
      {
        id: 0,
        label: 'Полный отчет'
      },
      {
        id: 1,
        label: 'Коммуникация'
      },
      {
        id: 2,
        label: 'Мышление'
      },
      {
        id: 3,
        label: 'Мотивация'
      }
    ];

    if(report) {
      let counter = 0;
      for(let key in report.toJS().list) {
        counter++
      }

      if(counter > 18) {
        reportShapeText.push({
          id: 4,
          label: 'Особенности'
        })
      }
    }

    return reportShapeText
  })()
})

const withReducer = injectReducer({ key: 'individualReport', reducer });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withRouter,
  withConnect,
  withReducer
)(ReportDropdown);
