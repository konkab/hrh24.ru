import React, { Fragment } from 'react';
import RootViews from 'navigation/routes/RootViews';
import SetActiveView from 'navigation/routes/SetActiveView';
import PaymentPage from 'pages/Settings/subpages/Payment';

const SettingsPaymentView = () => (
  <Fragment>
    <RootViews />
    <SetActiveView id="root/settings" />
    <PaymentPage />
  </Fragment>
);

export default SettingsPaymentView;
