import _objectSpread from '@babel/runtime/helpers/objectSpread';
import { css as _css } from 'emotion';
import _classCallCheck from '@babel/runtime/helpers/classCallCheck';
import _createClass from '@babel/runtime/helpers/createClass';
import _possibleConstructorReturn from '@babel/runtime/helpers/possibleConstructorReturn';
import _getPrototypeOf from '@babel/runtime/helpers/getPrototypeOf';
import _inherits from '@babel/runtime/helpers/inherits';
import _defineProperty from '@babel/runtime/helpers/defineProperty';
import React, { Component } from 'react';
import { gridSize as gridSizeFn } from '@atlaskit/theme';
import Section from '../Section';
import SkeletonContainerHeader from '../SkeletonContainerHeader';
import SkeletonItem from '../SkeletonItem';
import { ProductNavigationTheme, ContainerNavigationTheme } from '../ContentNavigation/primitives';
var gridSize = gridSizeFn();

var SkeletonContainerView =
  /*#__PURE__*/
  (function(_Component) {
    _inherits(SkeletonContainerView, _Component);

    function SkeletonContainerView() {
      _classCallCheck(this, SkeletonContainerView);

      return _possibleConstructorReturn(
        this,
        _getPrototypeOf(SkeletonContainerView).apply(this, arguments)
      );
    }

    _createClass(SkeletonContainerView, [
      {
        key: 'render',
        value: function render() {
          var type = this.props.type;

          if (!type) {
            return null;
          }

          var Wrapper = type === 'product' ? ProductNavigationTheme : ContainerNavigationTheme;
          return React.createElement(
            Wrapper,
            null,
            React.createElement(Section, null, function(_ref) {
              var css = _ref.css;
              return React.createElement(
                'div',
                {
                  className: _css(
                    _objectSpread({}, css, {
                      paddingTop: gridSize * 2.5,
                      paddingBottom: gridSize * 2.5
                    })
                  )
                },
                React.createElement(SkeletonContainerHeader, {
                  hasBefore: true
                })
              );
            }),
            React.createElement(Section, null, function(_ref2) {
              var className = _ref2.className;
              return React.createElement(
                'div',
                {
                  className: className
                },
                React.createElement(SkeletonItem, {
                  hasBefore: true
                }),
                React.createElement(SkeletonItem, {
                  hasBefore: true
                }),
                React.createElement(SkeletonItem, {
                  hasBefore: true
                }),
                React.createElement(SkeletonItem, {
                  hasBefore: true
                }),
                React.createElement(SkeletonItem, {
                  hasBefore: true
                })
              );
            })
          );
        }
      }
    ]);

    return SkeletonContainerView;
  })(Component);

_defineProperty(SkeletonContainerView, 'type', 'product');

export { SkeletonContainerView as default };
