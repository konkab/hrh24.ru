import { css as _css } from 'emotion';
import React from 'react';
import GlobalNavigationSkeletonItem from './GlobalNavigationSkeletonItem';
import { PrimaryItemsList, SecondaryItemsList, FirstPrimaryItemWrapper } from './primitives';

var GlobalNavigationSkeleton = function GlobalNavigationSkeleton(_ref) {
  var theme = _ref.theme;
  var wrapperStyles = theme.mode.globalNav();
  return React.createElement(
    'div',
    {
      className: _css(wrapperStyles)
    },
    React.createElement(
      PrimaryItemsList,
      null,
      React.createElement(
        FirstPrimaryItemWrapper,
        null,
        React.createElement(GlobalNavigationSkeletonItem, null)
      ),
      React.createElement(GlobalNavigationSkeletonItem, null),
      React.createElement(GlobalNavigationSkeletonItem, null),
      React.createElement(GlobalNavigationSkeletonItem, null)
    ),
    React.createElement(
      SecondaryItemsList,
      null,
      React.createElement(GlobalNavigationSkeletonItem, null),
      React.createElement(GlobalNavigationSkeletonItem, null),
      React.createElement(GlobalNavigationSkeletonItem, null),
      React.createElement(GlobalNavigationSkeletonItem, null)
    )
  );
};

export default GlobalNavigationSkeleton;
