import { css as _css2 } from 'emotion';
import _objectWithoutProperties from '@babel/runtime/helpers/objectWithoutProperties';
import _extends from '@babel/runtime/helpers/extends';
import { css as _css, cx as _cx } from 'emotion';
import React from 'react';
import { layers } from '@atlaskit/theme';
var _ref = {
  display: 'flex',
  flexDirection: 'row',
  height: '100vh'
};
export var LayoutContainer = function LayoutContainer(props) {
  return React.createElement(
    'div',
    _extends(
      {
        className: _css(_ref)
      },
      props
    )
  );
};
export var NavigationContainer = function NavigationContainer(_ref2) {
  var innerRef = _ref2.innerRef,
    props = _objectWithoutProperties(_ref2, ['innerRef']);

  return React.createElement(
    'div',
    _extends(
      {
        ref: innerRef,
        className: _cx(
          'navigation',
          _css2({
            bottom: 0,
            display: 'flex',
            flexDirection: 'row',
            left: 0,
            position: 'fixed',
            top: 0,
            zIndex: layers.navigation()
          })
        )
      },
      props
    )
  );
}; // Resizable Elements can be disabled
