import React from 'react';
import { shallow } from 'enzyme';

import Arrow from '../Arrow';

describe('<Arrow />', () => {
  it('should render Arrow', () => {
    const answer = shallow(<Arrow />);

    expect(answer).toMatchSnapshot();
  });
});
