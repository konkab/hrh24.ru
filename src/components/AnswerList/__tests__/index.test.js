import React from 'react';
import { shallow } from 'enzyme';
import Reorder from 'react-reorder';

import AnswerList from '../index';

describe('<AnswerList />', () => {
  it('should render empty AnswerList', () => {
    const renderedComponent = shallow(<AnswerList />);

    expect(renderedComponent.contains(<Reorder />)).toBe(false);
  });

  it('should render non-empty AnswerList', () => {
    const answers = [
      {
        text: 'Вопрос №1'
      },
      {
        text: 'Вопрос №2'
      },
      {
        text: 'Вопрос № 3'
      }
    ];
    const renderedComponent = shallow(<AnswerList answers={answers} />);

    expect(renderedComponent.find('Reorder').props().reorderId).toBe('my-list');
    expect(renderedComponent.find('Reorder').props().reorderGroup).toBe('reorder-group');
    expect(renderedComponent.find('Reorder').props().draggedClassName).toBe('dragged');
    expect(renderedComponent.state('list')).toBe(answers);
  });
});
