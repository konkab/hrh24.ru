import React from 'react';
import { shallow } from 'enzyme';

import Answer from '../Answer';

describe('<Answer />', () => {
  it('should render empty Answer', () => {
    const answer = shallow(<Answer />);

    expect(answer).toMatchSnapshot();
  });

  it('should render non-empty Answer', () => {
    const answer = { text: 'Answer number 1' };
    const component = shallow(<Answer answer={answer} />);

    expect(component).toMatchSnapshot();
  });
});
