import React from 'react';
import { mount, shallow } from 'enzyme';
import sinon from 'sinon';

import Progress from '../index';
import Cursor from '../styled/Cursor';
import ProgressAfter from '../styled/ProgressAfter';
import ProgressBefore from '../styled/ProgressBefore';

describe('<Progress />', () => {
  it('should throw an error if render Progress without props', () => {
    const stub = sinon.stub(console, 'error');
    const component = shallow(<Progress />);

    expect(stub.called).toBe(true);
    expect(component).toMatchSnapshot();
  });

  it('should render Progress', () => {
    const after = 30;
    const before = 20;
    const onAfterChange = jest.fn();
    const onBeforeChange = jest.fn();
    const patternId = 1;
    const programId = 1;

    const component = shallow(
      <Progress
        after={after}
        before={before}
        onAfterChange={onAfterChange}
        onBeforeChange={onBeforeChange}
        patternId={patternId}
        programId={programId}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('should call onBeforeChange on beforeCursor move', () => {
    const after = 30;
    const before = 20;
    const onAfterChange = jest.fn();
    const onBeforeChange = jest.fn();
    const patternId = 1;
    const programId = 2;

    const map = {};
    document.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });

    const component = mount(
      <Progress
        after={after}
        before={before}
        onAfterChange={onAfterChange}
        onBeforeChange={onBeforeChange}
        patternId={patternId}
        programId={programId}
      />
    );

    const cursor = component.find(ProgressBefore).find(Cursor);
    cursor.simulate('mousedown');
    map.mousemove({ screenX: 100, screenY: 0 });
    map.mouseup();

    expect(component.state('before')).toBe(65);
    expect(onBeforeChange).toBeCalledWith(programId, patternId, 65);
  });

  it('should call onAfterChange on afterCursor move', () => {
    const after = 30;
    const before = 20;
    const onAfterChange = jest.fn();
    const onBeforeChange = jest.fn();
    const patternId = 1;
    const programId = 2;

    const map = {};
    document.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });

    const component = mount(
      <Progress
        after={after}
        before={before}
        onAfterChange={onAfterChange}
        onBeforeChange={onBeforeChange}
        patternId={patternId}
        programId={programId}
      />
    );

    const cursor = component.find(ProgressAfter).find(Cursor);
    cursor.simulate('mousedown');
    map.mousemove({ screenX: 100, screenY: 0 });
    map.mouseup();

    expect(component.state('after')).toBe(0);
    expect(onAfterChange).toBeCalledWith(programId, patternId, 100);
  });
});
