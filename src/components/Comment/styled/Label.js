import styled from 'styled-components';

const Label = styled.label`
  color: #6c798f;
  font-size: 12px;
  font-weight: 600;
  letter-spacing: -0.1px;
  line-height: 16px;
  text-align: left;
`;

export default Label;
