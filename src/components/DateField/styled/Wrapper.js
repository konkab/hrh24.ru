import styled from 'styled-components';

const Wrapper = styled.div`
  flex: 1 1 100%;
`;

export default Wrapper;
