import React from 'react';
import { shallow } from 'enzyme';

import Modal from '../index';

describe('<Modal />', () => {
  it('should render empty div if render Modal without actions', () => {
    const component = shallow(<Modal />);

    expect(component.contains(<div />)).toBe(true);
    expect(component).toMatchSnapshot();
  });

  it('should render Modal', () => {
    const actions = [
      {
        text: 'Action 1',
        onClick: jest.fn()
      },
      {
        text: 'Action 2',
        onClick: jest.fn()
      }
    ];

    const component = shallow(<Modal actions={actions} />);
    expect(component).toMatchSnapshot();
  });
});
