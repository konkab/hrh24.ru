import styled from 'styled-components';

const ButtonWrapper = styled.div`
  margin-top: 10px;
`;

export default ButtonWrapper;
