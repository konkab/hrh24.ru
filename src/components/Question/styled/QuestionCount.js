import styled from 'styled-components';

const QuestionCount = styled.div`
  margin-bottom: 21px;
  display: flex;
  justify-content: center;
`;

export default QuestionCount;
