import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import Question from '../index';

describe('<Question />', () => {
  it('should throw error if render empty Question', () => {
    const stub = sinon.stub(console, 'error');
    const component = shallow(<Question />);

    expect(stub.called).toBe(true);
    expect(component).toMatchSnapshot();
  });

  it('should render Question', () => {
    const currentQuestionNumber = 5;
    const onAnswersChange = jest.fn();
    const question = {
      body: 'Question text',
      answers: []
    };
    const totalQuestionNumber = 30;

    const component = shallow(
      <Question
        currentQuestionNumber={currentQuestionNumber}
        onAnswersChange={onAnswersChange}
        question={question}
        totalQuestionNumber={totalQuestionNumber}
      />
    );
    expect(component).toMatchSnapshot();
  });
});
