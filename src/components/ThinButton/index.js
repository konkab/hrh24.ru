import styled from 'styled-components';

const ThinButton = styled.button`
  align-items: baseline;
  background: rgb(0, 82, 204);
  border-radius: 3px;
  border-width: 0;
  box-sizing: border-box;
  color: #ffffff !important;
  cursor: default;
  display: inline-flex;
  font-size: inherit;
  font-style: normal;
  height: 24px;
  line-height: 24px;
  margin: 0;
  max-width: 100%;
  outline: none !important;
  padding: 0 8px;
  text-align: center;
  text-decoration: none;
  transition: background 0.1s ease-out, box-shadow 0.15s cubic-bezier(0.47, 0.03, 0.49, 1.38);
  transition-duration: 0.1s, 0.15s;
  vertical-align: middle;
  white-space: nowrap;
  width: auto;

  &:hover {
    background: rgb(0, 101, 255);
  }

  &:active {
    background: rgb(7, 71, 166);
  }
`;

export default ThinButton;
