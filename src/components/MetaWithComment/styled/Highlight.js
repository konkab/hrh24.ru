import styled from 'styled-components';

const Highlight = styled.div`
  color: transparent;
  cursor: pointer;
  position: absolute;
  font-size: 14px;
  letter-spacing: -0.15px;
  text-align: left;
  line-height: 20px;
  left: 0;
  top: 0;
  z-index: 0;

  & span {
    background-color: #fff78f;
  }
`;

export default Highlight;
