import React from 'react';
import { shallow } from 'enzyme';
import Center from '../index';

describe('<Center />', () => {
  it('should render Center', () => {
    expect(shallow(<Center />)).toMatchSnapshot();
  });
});
