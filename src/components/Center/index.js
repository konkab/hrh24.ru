import styled from 'styled-components';

const Center = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

export default Center;
