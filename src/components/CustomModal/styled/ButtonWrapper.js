import styled from 'styled-components';

const ButtonWrapper = styled.div`
  margin-left: auto;
  margin-top: 22px;
`;

export default ButtonWrapper;
