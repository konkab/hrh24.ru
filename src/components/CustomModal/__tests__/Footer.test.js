import React from 'react';
import { shallow } from 'enzyme';

import Footer from '../Footer';

describe('<Footer /> in CustomModal', () => {
  it('should render Footer', () => {
    const component = shallow(<Footer />);

    expect(component).toMatchSnapshot();
  });

  // it('should')
});
