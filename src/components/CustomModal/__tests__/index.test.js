import React from 'react';
import { shallow } from 'enzyme';

import CustomModal from '../index';

describe('<CustomModal />', () => {
  it('should render CustomModal', () => {
    const component = shallow(<CustomModal />);

    expect(component).toMatchSnapshot();
  });
});
