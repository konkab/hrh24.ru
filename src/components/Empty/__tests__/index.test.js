import React from 'react';
import { shallow } from 'enzyme';
import Empty from '../index';

describe('<Empty />', () => {
  it('should render Empty', () => {
    const component = shallow(<Empty />);

    expect(component).toMatchSnapshot();
  });
});
