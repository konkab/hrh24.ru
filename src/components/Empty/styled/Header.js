import styled from 'styled-components';

const Header = styled.h3`
  max-width: 312px;
  text-align: center;
`;

export default Header;
