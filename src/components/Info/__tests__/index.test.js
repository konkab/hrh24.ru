import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import Info from '../index';

describe('<Info />', () => {
  it('should throw an error if render empty Info', () => {
    const stub = sinon.stub(console, 'error');
    const component = shallow(<Info />);

    expect(stub.calledOnce).toBe(true);
    expect(component).toMatchSnapshot();
  });

  it('should render Info', () => {
    const text = 'Info text';
    const component = shallow(<Info text={text} />);

    expect(component).toMatchSnapshot();
  });

  it('should toggle isOpened state on mouseover/mouseleave', () => {
    const text = 'Info text';
    const component = shallow(<Info text={text} />);

    expect(component.state('isOpened')).toBe(false);

    component.find('.info-icon').simulate('mouseover');
    expect(component.state('isOpened')).toBe(true);

    component.find('.info-icon').simulate('mouseleave');
    expect(component.state('isOpened')).toBe(false);
  });
});
