import styled from 'styled-components';

const Icon = styled.img`
  cursor: pointer;
  opacity: ${props => (props.isOpened ? '1' : '.5')};
  transition: opacity 0.3s ease-out;
`;

export default Icon;
