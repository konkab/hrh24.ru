import styled from 'styled-components';

const Rouble = styled.span`
  font-family: 'Roboto', sans-serif;
`;

export default Rouble;
