import React from 'react';
import { shallow } from 'enzyme';

import OnlyMobile from '../index';

describe('<OnlyMobile />', () => {
  it('should render OnlyMobile', () => {
    const component = shallow(<OnlyMobile />);
    expect(component).toMatchSnapshot();
  });
});
