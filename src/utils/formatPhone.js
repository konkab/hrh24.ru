export const formatPhone = value => {
  return `+${value.replace(/[()-/\s]/g, '')}`;
};
