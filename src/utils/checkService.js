const service =
  process.env.REACT_APP_TARGET === "lk.my-yadro.ru" ? "my-yadro" : "hrh24";

export const serviceName = service === "my-yadro" ? "Ядро личности" : "HRHelper";
export const legalName = service === "my-yadro" ? "ИП Лузянина Виктория Павловна" : "ИП Гайдук Егор Александрович";

export default service;
