import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  box-shadow: 0 10px 30px 0 rgba(18, 43, 72, 0.12);
  border-radius: 20px;
  padding: 50px 20px;
  margin-right: 40px;
  margin-top: 50px;
  width: 320px;

  &:nth-child(3n) {
    margin-right: 0;
  }

  @media (max-width: 1200px) and (min-width: 1021px) {
    margin-right: 15px;
    padding: 40px 20px;
    width: calc((100% - 30px) / 3 - 40px);

    &:nth-child(3n) {
      margin-right: 0;
    }
  }

  @media (max-width: 1020px) {
    margin-right: 0;
    padding: 30px 20px;
    width: 100%;

    &:first-child {
      margin-top: 0;
    }
  }
`;

const Title = styled.p`
  font-size: 24px;
  color: #0052cc;
  letter-spacing: 0.33px;
  text-align: center;
  line-height: 28px;
  font-weight: 500;

  @media (max-width: 1020px) {
    font-size: 19px;
    line-height: 22px;
  }
`;

const Item = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 30px 0;
  text-align: center;

  &:first-child {
    padding-top: 60px;
  }

  &:last-child {
    padding-bottom: 60px;
  }
`;

const ItemTitle = styled.div`
  font-size: 16px;
  color: #172b4d;
  letter-spacing: -0.32px;
  line-height: 24px;

  @media (max-width: 1020px) {
    font-size: 14px;
    line-height: 20px;
  }
`;

const ItemCount = styled.div`
  color: #172b4d;
  font-size: 29px;
  font-weight: 500;
  text-align: right;
  letter-spacing: 0.32px;
  line-height: 32px;

  @media (max-width: 1020px) {
    font-size: 22px;
    line-height: 26px;
  }
`;

const Rouble = styled.span`
  font-family: 'Roboto', sans-serif;
`;

const Card = props => (
  <Wrapper>
    <Title>{props.title}</Title>
    <div>
      <Item>
        <ItemTitle>Включено отчетов</ItemTitle>
        <ItemCount>{props.reports}</ItemCount>
      </Item>
      <Item>
        <ItemTitle>Каждый дополнительный отчет</ItemTitle>
        <ItemCount>
          от {props.advanced} <Rouble>&#8381;</Rouble>
        </ItemCount>
      </Item>
      <Item>
        <ItemTitle>Абонентская плата в год</ItemTitle>
        <ItemCount>
          {props.price} <Rouble>&#8381;</Rouble>
        </ItemCount>
      </Item>
    </div>
  </Wrapper>
);

export default Card;
