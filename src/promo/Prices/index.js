import React from 'react';
import styled from 'styled-components';
import Card from './Card';

const Wrapper = styled.section`
  padding: 100px 0 100px;

  @media (max-width: 1020px) {
    padding: 50px 0 0;
  }
`;

const Inner = styled.div`
  @media (max-width: 1020px) {
    margin: 0 auto;
    width: calc(100% - 30px);
  }
`;

const Title = styled.p`
  color: #172b4d;
  font-size: 29px;
  letter-spacing: 0.32px;
  line-height: 32px;
  text-align: center;

  @media (max-width: 1020px) {
    font-size: 22px;
    line-height: 26px;
    text-align: left;
    width: 100%;
  }
`;

const CardsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 50px auto 0;
  width: 1160px;

  @media (max-width: 1200px) and (min-width: 1021px) {
    width: calc(100% - 30px);
  }

  @media (max-width: 1020px) {
    margin: 50px auto;
    width: 100%;
  }
`;

const Prices = () => (
  <Wrapper>
    <Inner>
      <Title>Сколько это стоит?</Title>
      <CardsWrapper>
        <Card title="Стартап" reports="0" advanced="300" price="0" />
        <Card title="Бизнес" reports="50" advanced="200" price="10 000" />
        <Card title="Корпорация" reports="250" advanced="150" price="37 500" />
      </CardsWrapper>
    </Inner>
  </Wrapper>
);

export default Prices;
