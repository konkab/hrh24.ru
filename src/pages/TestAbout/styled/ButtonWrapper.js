import styled from 'styled-components';

const ButtonWrapper = styled.div`
  margin: 50px 0;
  text-align: center;
`;

export default ButtonWrapper;
