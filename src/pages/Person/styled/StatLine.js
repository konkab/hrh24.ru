import styled from 'styled-components';

const StatLine = styled.div`
  margin: 20px 0;
`;

export default StatLine;
