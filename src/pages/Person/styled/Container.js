import styled from 'styled-components';

const Container = styled.div`
  max-width: 640px;
  margin: 0 auto;
  padding: 0px 2rem;
`;

export default Container;
