import React from 'react';

const IconReport = () => (
  <div>
    <svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation">
      <g fill="none" fillRule="evenodd">
        <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <g id="091" transform="translate(-95.000000, -134.000000)" fill="#42526E">
            <g id="side_menu">
              <g id="icon_report" transform="translate(95.000000, 134.000000)">
                <path
                  d="M5,2 L5,13 L15,13 C15.5522847,13 16,12.5522847 16,12 L16,2 L5,2 Z M5,0 L16,0 C17.1045695,6.76353751e-17 18,0.8954305 18,2 L18,12 C18,13.6568542 16.6568542,15 15,15 L5,15 C3.8954305,15 3,14.1045695 3,13 L3,2 C3,0.8954305 3.8954305,2.02906125e-16 5,0 Z"
                  id="Shape"
                />
                <path
                  d="M7,4 L14,4 L14,6 L7,6 L7,4 Z M7,8 L14,8 L14,10 L7,10 L7,8 Z M8,13 L8,15 L2,15 C0.8954305,15 1.3527075e-16,14.1045695 0,13 L0,4 C-6.76353751e-17,3.44771525 0.44771525,3 1,3 C1.55228475,3 2,3.44771525 2,4 L2,12.5 C2,12.7761424 2.22385763,13 2.5,13 C2.77614237,13 3,12.7761424 3,12.5 L3,12 L5,12 L5,13 L8,13 Z"
                  id="Shape"
                />
                <rect id="Rectangle-path" x="0" y="3" width="5" height="2" rx="1" />
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  </div>
);

export default IconReport;
