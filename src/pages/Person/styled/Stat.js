import styled from 'styled-components';

const Stat = styled.div`
  margin: 45px 0;
`;

export default Stat;
