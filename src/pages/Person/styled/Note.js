import styled from 'styled-components';

const Note = styled.p`
  color: #6b778c;
  font-size: 12px;
  margin: 5px 0;
`;

export default Note;
