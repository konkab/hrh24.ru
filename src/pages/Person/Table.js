import React from 'react';
import DynamicTable from '@atlaskit/dynamic-table';
import DropdownMenu, { DropdownItemGroup, DropdownItem } from '@atlaskit/dropdown-menu';
import NormalButton from '@atlaskit/button';
import NormalFlag, { FlagGroup } from '@atlaskit/flag';
import Tick from '@atlaskit/icon/glyph/check-circle';
import styled from 'styled-components';
import Form from '@atlaskit/form';
import TextField from '@atlaskit/field-text';
import TextArea from '@atlaskit/field-text-area';

import Modal from 'components/Modal';
import data from './data.json';

function createKey(input) {
  return input ? input.replace(/^(the|a|an)/, '').replace(/\s/g, '') : input;
}

const NameWrapper = styled.span`
  display: flex;
  align-items: center;
  height: 40px;
`;

const Flag = styled(NormalFlag)`
  background-color: green !important;
`;

const FieldGroup = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: ${props => (props.marginTop ? props.marginTop : 'inherit')};
`;

const Field = styled.div`
  align-self: ${props => (props.alignSelf ? props.alignSelf : 'inherit')};
  width: ${props => (props.width ? props.width : 'calc(50% - 10px)')};
`;

const Button = styled(NormalButton)`
  &.allWidth {
    display: flex;
    height: 40px;
    justify-content: center;
    text-align: center;
    width: 100%;
  }
`;

const Text = styled.p`
  align-self: center;
  margin: 20px 0 0;
`;

const Line = styled.div`
  align-self: center;
  background-color: #e2e3e7;
  height: 1px;
  margin: 20px 0 0;
  width: 150px;
`;

const TriggerButton = <Button>...</Button>;

const createHead = withWidth => {
  return {
    cells: [
      {
        key: 'name',
        content: 'Имя',
        isSortable: true,
        width: withWidth ? 27 : undefined
      },
      {
        key: 'party',
        content: 'Email',
        shouldTruncate: true,
        isSortable: false,
        width: withWidth ? 25 : undefined
      },
      {
        key: 'term',
        content: 'Дата тестирования',
        shouldTruncate: true,
        isSortable: true,
        width: withWidth ? 22 : undefined
      },
      {
        key: 'content',
        content: 'HR-менеджер',
        shouldTruncate: true,
        width: withWidth ? 22 : undefined
      },
      {
        key: 'more',
        shouldTruncate: true
      }
    ]
  };
};

const headTable = createHead(true);

class Table extends React.Component {
  state = {
    deleteModals: [],
    sendModals: [],
    flags: [],
    sendFlags: []
  };

  addFlag = id => {
    this.setState({
      flags: [this.state.flags.length, ...this.state.flags],
      deleteModals: this.state.deleteModals.filter(i => i !== id)
    });
  };

  removeFlag = id => this.setState({ flags: this.state.flags.filter(v => v !== id) });

  closeModal = id => {
    this.setState({ deleteModals: this.state.deleteModals.filter(i => i !== id) });
  };

  closeEditModal = id => {
    this.setState({ sendModals: this.state.sendModals.filter(i => i !== id) });
  };

  addSendFlag = id => {
    this.setState({
      sendFlags: [this.state.flags.length, ...this.state.flags],
      sendModals: this.state.sendModals.filter(i => i !== id)
    });
  };

  removeSendFlag = id => this.setState({ sendFlags: this.state.sendFlags.filter(v => v !== id) });

  render() {
    const rowsTable = data.map((item, index) => ({
      key: `row-${index}-${item.id}`,
      cells: [
        {
          key: createKey(item.name),
          content: (
            <NameWrapper>
              <a href="/personal">{item.name}</a>
            </NameWrapper>
          )
        },
        {
          key: createKey(item.email),
          content: item.email
        },
        {
          key: createKey(item.result),
          content: item.result
        },
        {
          key: createKey(item.hr),
          content: item.hr
        },
        {
          content: (
            <DropdownMenu trigger={TriggerButton} position="bottom right">
              <DropdownItemGroup>
                <DropdownItem
                  onClick={() =>
                    this.setState({
                      deleteModals: [1]
                    })
                  }
                >
                  Удалить
                </DropdownItem>
                <DropdownItem>Изменить</DropdownItem>
                <DropdownItem
                  onClick={() =>
                    this.setState({
                      sendModals: [1]
                    })
                  }
                >
                  Отправить тест
                </DropdownItem>
              </DropdownItemGroup>
            </DropdownMenu>
          )
        }
      ]
    }));

    return (
      <div>
        <DynamicTable
          head={headTable}
          rows={rowsTable}
          rowsPerPage={25}
          defaultPage={1}
          loadingSpinnerSize="large"
          isLoading={false}
          defaultSortKey="term"
          defaultSortOrder="ASC"
          paginationi18n={{ prev: 'Предыдущая', next: 'Следующая' }}
        />

        {this.state.deleteModals.map(id => (
          <Modal
            key={id}
            id={id}
            heading="Удалить сотрудника"
            onClose={this.closeModal}
            actions={[
              {
                text: 'Удалить',
                onClick: this.addFlag
              },
              {
                text: 'Отменить',
                onClick: this.closeModal
              }
            ]}
          >
            <p>Уверены, что хотите удалить сотрудника?</p>
          </Modal>
        ))}

        {this.state.sendModals.map(id => (
          <Modal
            key={id}
            id={id}
            heading="Отправить тест"
            onClose={this.closeEditModal}
            actions={[
              {
                text: 'Отправить',
                onClick: this.addSendFlag
              },
              {
                text: 'Отменить',
                onClick: this.closeEditModal
              }
            ]}
            width="small"
          >
            <p>Отправьте или скопируйте ссылку</p>
            <Form name="layout-example" onSubmit={() => {}} onReset={() => {}} method="GET">
              <FieldGroup layout="column" form="">
                <Field width="65%">
                  <TextField
                    label="Скопируйте ссылку"
                    shouldFitContainer
                    name="link"
                    placeholder=""
                    isReadOnly
                    value="http://profgrowth.com/test/12345t"
                  />
                </Field>
                <Field width="32%" alignSelf="flex-end">
                  <Button className="allWidth">Скопировать</Button>
                </Field>
              </FieldGroup>
              <FieldGroup>
                <Line />
                <Text>или</Text>
                <Line />
              </FieldGroup>
              <FieldGroup layout="column" form="">
                <Field width="100%">
                  <TextField
                    label="Отправьте тестирование"
                    shouldFitContainer
                    name="email"
                    placeholder=""
                    value="petrov@mail.ru"
                  />
                </Field>
              </FieldGroup>
              <FieldGroup marginTop="8px">
                <Field width="100%">
                  <TextArea
                    shouldFitContainer
                    name="message"
                    placeholder="Добавьте сообщение (необязательно)"
                    value=""
                    isLabelHidden
                  />
                </Field>
              </FieldGroup>
            </Form>
          </Modal>
        ))}

        <FlagGroup onDismissed={name => this.removeFlag(name)}>
          {this.state.flags.map(id => (
            <Flag
              isDismissAllowed
              id={id}
              icon={<Tick label="Success" />}
              key={`${id}`}
              title="Сотрудник удален"
            />
          ))}
        </FlagGroup>

        <FlagGroup onDismissed={name => this.removeSendFlag(name)}>
          {this.state.sendFlags.map(id => (
            <Flag
              isDismissAllowed
              id={id}
              icon={<Tick label="Success" />}
              key={`${id}`}
              title="Ссылка отправлена"
            />
          ))}
        </FlagGroup>
      </div>
    );
  }
}

export default Table;
