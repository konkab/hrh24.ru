import styled from 'styled-components';

const FieldGroup = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: ${props => (props.marginTop ? props.marginTop : 'inherit')};
`;

export default FieldGroup;
