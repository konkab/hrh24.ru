import styled from 'styled-components';

const P = styled.p`
  margin-top: 16px;
  text-align: center;
`;

export default P;
