import styled from 'styled-components';

const ButtonWrapper = styled.div`
  display: flex;
  margin: 40px auto;

  & > div {
    min-width: 170px;

    &:first-child {
      margin-right: 15px;
    }
  }
`;

export default ButtonWrapper;
