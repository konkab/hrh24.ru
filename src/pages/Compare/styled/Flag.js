import NormalFlag from '@atlaskit/flag';
import styled from 'styled-components';

const Flag = styled(NormalFlag)`
  background-color: green !important;
`;

export default Flag;
