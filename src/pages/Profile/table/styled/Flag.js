import styled from 'styled-components';
import NormalFlag from '@atlaskit/flag';

const Flag = styled(NormalFlag)`
  background-color: green !important;
`;

export default Flag;
