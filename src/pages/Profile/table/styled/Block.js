import styled from 'styled-components';

const Block = styled.div`
  margin: 15px 0;
`;

export default Block;
