import { call, select, put, takeLeading } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import axios from 'axios';
import apiUrl, { pageHostUrl } from 'utils/serverConfig';

import { getUserInfo } from 'pages/Home/actions';

import { LOGIN_REQUEST, LOGOUT, SEND_OTP } from './constants';

import { loginSuccess, loginFail, sendOtpSuccess, sendOtpFail } from './actions';

import { makeSelectLogin, makeSelectPassword } from './selectors';

function* loginSaga() {
  const url = `${apiUrl}/api/managers/auth/login/`;
  const login = yield select(makeSelectLogin());
  const password = yield select(makeSelectPassword());
  const options = {
    method: 'post',
    data: {
      email: login,
      password: password
    }
  };

  try {
    const request = yield call(axios, url, options);

    const key = request.data.key;
    localStorage.setItem('key', key);

    yield put(loginSuccess());
    yield put(getUserInfo());
    yield put(push('/invites'));
  } catch (e) {
    const errorMessage = e.response.data.non_field_errors[0];
    yield put(
      loginFail({
        errorMessage
      })
    );
  }
}

function logout() {
  localStorage.removeItem('key');
  window.location.href = `${pageHostUrl}/signin`;
}

function* sendOtpSaga({ otp }) {
  const url = `${apiUrl}/api/managers/auth/login/otp`;
  const options = {
    method: 'post',
    data: {
      otp
    }
  };

  try {
    const request = yield call(axios, url, options);
    localStorage.setItem('key', request.data.key);
    yield put(sendOtpSuccess());
    yield put(push('/invites'));
  } catch (e) {
    yield put(sendOtpFail());
  }
}

export default function* loginPageSaga() {
  yield takeLeading(LOGIN_REQUEST, loginSaga);
  yield takeLeading(LOGOUT, logout);
  yield takeLeading(SEND_OTP, sendOtpSaga);
}
