import styled from 'styled-components';

const Link = styled.a`
  display: block;
  margin-top: 5px;
  outline: none !important;
`;

export default Link;
