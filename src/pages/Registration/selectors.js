import { createSelector } from 'reselect';

const selectGlobal = state => state.get('registration');

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('loading')
  );

const makeSelectRegisterLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('registerLoading')
  );

const makeSelectName = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('first_name')
  );

const makeSelectSurname = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('last_name')
  );

const makeSelectEmail = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('email')
  );

const makeSelectPhone = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('phone_number')
  );

const makeSelectPasswordOne = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('password1')
  );

const makeSelectSmsCode = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('smsCode')
  );

const makeSelectInvitationCode = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('invitationCode')
  );

const makeSelectErrorMessages = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('errorMessages').toJS()
  );

const makeSelectFlags = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('flags').toJS()
  );

const makeSelectSmsFlags = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('smsFlags').toJS()
  );

export {
  makeSelectLoading,
  makeSelectName,
  makeSelectSurname,
  makeSelectEmail,
  makeSelectPhone,
  makeSelectPasswordOne,
  makeSelectSmsCode,
  makeSelectInvitationCode,
  makeSelectErrorMessages,
  makeSelectRegisterLoading,
  makeSelectFlags,
  makeSelectSmsFlags
};
