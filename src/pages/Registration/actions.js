import {
  CHANGE_NAME,
  CHANGE_SURNAME,
  CHANGE_EMAIL,
  CHANGE_PHONE_NUMBER,
  CHANGE_PASSWORD_ONE,
  CHANGE_SMS_CODE,
  CHANGE_FLAGS,
  CHANGE_SMS_FLAGS,
  SEND_SMS_CODE_REQUEST,
  SEND_SMS_CODE_REQUEST_SUCCESS,
  SEND_SMS_CODE_REQUEST_FAIL,
  REGISTER_USER_REQUEST,
  REGISTER_USER_REQUEST_SUCCESS,
  REGISTER_USER_REQUEST_FAIL,
  CHECK_INVITE_CODE_REQUEST,
  CHECK_INVITE_CODE_REQUEST_SUCCESS,
  CHECK_INVITE_CODE_REQUEST_FAIL
} from './constants';

export function changeSmsFlags(payload) {
  return {
    type: CHANGE_SMS_FLAGS,
    ...payload
  };
}

export function sendSmsCode() {
  return {
    type: SEND_SMS_CODE_REQUEST
  };
}

export function sendSmsCodeSuccess() {
  return {
    type: SEND_SMS_CODE_REQUEST_SUCCESS
  };
}

export function sendSmsCodeFail() {
  return {
    type: SEND_SMS_CODE_REQUEST_FAIL
  };
}

export function changeName(payload) {
  return {
    type: CHANGE_NAME,
    ...payload
  };
}

export function changeSurname(payload) {
  return {
    type: CHANGE_SURNAME,
    ...payload
  };
}

export function changeEmail(payload) {
  return {
    type: CHANGE_EMAIL,
    ...payload
  };
}

export function changePhone(payload) {
  return {
    type: CHANGE_PHONE_NUMBER,
    ...payload
  };
}

export function changePasswordOne(payload) {
  return {
    type: CHANGE_PASSWORD_ONE,
    ...payload
  };
}

export function changeSmsCode(payload) {
  return {
    type: CHANGE_SMS_CODE,
    ...payload
  };
}

export function registerUser() {
  return {
    type: REGISTER_USER_REQUEST
  };
}

export function registerUserSuccess() {
  return {
    type: REGISTER_USER_REQUEST_SUCCESS
  };
}

export function registerUserFail(payload) {
  return {
    type: REGISTER_USER_REQUEST_FAIL,
    ...payload
  };
}

export function checkInviteCode(payload) {
  return {
    type: CHECK_INVITE_CODE_REQUEST,
    ...payload
  };
}

export function checkInviteCodeSuccess() {
  return {
    type: CHECK_INVITE_CODE_REQUEST_SUCCESS
  };
}

export function checkInviteCodeFail() {
  return {
    type: CHECK_INVITE_CODE_REQUEST_FAIL
  };
}

export function changeFlags(payload) {
  return {
    type: CHANGE_FLAGS,
    ...payload
  };
}
