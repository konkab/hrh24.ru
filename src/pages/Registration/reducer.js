import { fromJS } from 'immutable';

import {
  CHANGE_NAME,
  CHANGE_SURNAME,
  CHANGE_EMAIL,
  CHANGE_PHONE_NUMBER,
  CHANGE_PASSWORD_ONE,
  CHANGE_SMS_CODE,
  CHANGE_FLAGS,
  CHANGE_SMS_FLAGS,
  SEND_SMS_CODE_REQUEST,
  SEND_SMS_CODE_REQUEST_SUCCESS,
  SEND_SMS_CODE_REQUEST_FAIL,
  CHECK_INVITE_CODE_REQUEST,
  CHECK_INVITE_CODE_REQUEST_SUCCESS,
  CHECK_INVITE_CODE_REQUEST_FAIL,
  REGISTER_USER_REQUEST,
  REGISTER_USER_REQUEST_SUCCESS,
  REGISTER_USER_REQUEST_FAIL
} from './constants';

const initialState = fromJS({
  loading: false,
  registerLoading: false,
  first_name: '',
  last_name: '',
  email: '',
  phone_number: '',
  password1: '',
  smsCode: '',
  invitationCode: '',
  errorMessages: {
    email: '',
    phone_number: ''
  },
  flags: [],
  smsFlags: []
});

function registrationReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_NAME:
      return state.set('first_name', action.name);
    case CHANGE_SURNAME:
      return state.set('last_name', action.surname);
    case CHANGE_EMAIL:
      return state.set('email', action.email);
    case CHANGE_PHONE_NUMBER:
      return state.set('phone_number', action.phone);
    case CHANGE_PASSWORD_ONE:
      return state.set('password1', action.password);
    case CHANGE_SMS_CODE:
      return state.set('smsCode', action.smsCode);
    case CHANGE_FLAGS:
      return state.set('flags', fromJS(action.flags));
    case CHANGE_SMS_FLAGS:
      return state.set('smsFlags', fromJS(action.smsFlags));
    case SEND_SMS_CODE_REQUEST:
      return state.set('smsFlags', fromJS([]));
    case SEND_SMS_CODE_REQUEST_SUCCESS:
      return state.set('smsFlags', fromJS([1]));
    case SEND_SMS_CODE_REQUEST_FAIL:
      return state.set('smsFlags', fromJS([0]));
    case REGISTER_USER_REQUEST:
      return state
        .set('registerLoading', true)
        .setIn(['errorMessages', 'email'], '')
        .setIn(['errorMessages', 'phone_number'], '');
    case REGISTER_USER_REQUEST_SUCCESS:
      return state.set('registerLoading', false);
    case REGISTER_USER_REQUEST_FAIL:
      return state
        .set('registerLoading', false)
        .setIn(['errorMessages', 'email'], action.email)
        .setIn(['errorMessages', 'phone_number'], action.phone_number)
        .set('flags', fromJS([1]));
    case CHECK_INVITE_CODE_REQUEST:
      return state.set('invitationCode', action.invitationCode).set('loading', true);
    case CHECK_INVITE_CODE_REQUEST_SUCCESS:
      return state.set('loading', false);
    case CHECK_INVITE_CODE_REQUEST_FAIL:
      return state.set('loading', false);
    default:
      return state;
  }
}

export default registrationReducer;
