import styled from 'styled-components';

const SelectWrapper = styled.div`
  margin-top: ${props => (props.marginTop ? props.marginTop : '25px')};
`;

export default SelectWrapper;
