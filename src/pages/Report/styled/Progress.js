import styled from 'styled-components';

const Progress = styled.span`
  position: relative;
  z-index: 10;
`;

export default Progress;
