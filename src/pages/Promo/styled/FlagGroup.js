import styled from 'styled-components';
import { FlagGroup as NormalGroup } from '@atlaskit/flag';

const FlagGroup = styled(NormalGroup)`
  width: auto !important;
`;

export default FlagGroup;
