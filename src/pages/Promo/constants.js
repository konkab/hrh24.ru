export const CHANGE_NAME = 'hh/pages/promo/CHANGE_NAME';
export const CHANGE_PHONE = 'hh/pages/promo/CHANGE_PHONE';

export const SEND_LEAD_REQUEST = 'hh/pages/promo/SEND_LEAD_REQUEST';
export const SEND_LEAD_REQUEST_SUCCESS = 'hh/pages/promo/SEND_LEAD_REQUEST_SUCCESS';
export const SEND_LEAD_REQUEST_FAIL = 'hh/pages/promo/SEND_LEAD_REQUEST_FAIL';
