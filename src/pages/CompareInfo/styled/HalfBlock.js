import styled from 'styled-components';

const HalfBlock = styled.div`
  width: calc(50% - 20px);
`;

export default HalfBlock;
