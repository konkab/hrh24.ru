import styled from 'styled-components';

const ProgressBefore = styled.div`
  position: absolute;
  font-size: 12px;
  font-weight: 500;
  color: #6b778c;
  letter-spacing: 0;
  line-height: 16px;
  bottom: -24px;
  left: ${props => `${props.width}%`};
  transform: translateX(-50%);
`;

export default ProgressBefore;
