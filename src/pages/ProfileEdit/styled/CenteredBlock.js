import styled from 'styled-components';

const CenteredBlock = styled.div`
  align-items: center;
  display: flex;
`;

export default CenteredBlock;
