import styled from 'styled-components';

const P = styled.p`
  color: ${props => (props.color ? props.color : 'inherit')};
  margin-bottom: ${props => (props.marginBottom ? props.marginBottom : 'inherit')};
  margin-top: ${props => (props.marginTop ? `${props.marginTop} !important` : 'inherit')};
`;

export default P;
