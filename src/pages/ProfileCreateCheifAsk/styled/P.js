import styled from 'styled-components';

const P = styled.p`
  color: ${props => (props.color ? props.color : 'initial')};
  margin-top: ${props => (props.marginTop ? props.marginTop : 'initial')};
`;

export default P;
