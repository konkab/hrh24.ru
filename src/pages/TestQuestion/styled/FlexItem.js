import styled from 'styled-components';

const FlexItem = styled.div`
  text-align: ${props => (props.align ? props.align : 'initial')};
  width: 100%;
`;

export default FlexItem;
