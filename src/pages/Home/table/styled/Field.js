import styled from 'styled-components';

const Field = styled.div`
  align-self: ${props => (props.alignSelf ? props.alignSelf : 'inherit')};
  width: ${props => (props.width ? props.width : 'calc(50% - 10px)')};
`;

export default Field;
