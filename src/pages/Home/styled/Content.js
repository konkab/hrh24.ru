import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div``;

const Content = ({ data, elementProps }) => <Wrapper {...elementProps}>{data.content}</Wrapper>;

export default Content;
