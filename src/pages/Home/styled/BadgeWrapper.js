import styled from 'styled-components';

const BadgeWrapper = styled.span`
  display: inline-block;
  margin-left: 10px;
`;

export default BadgeWrapper;
