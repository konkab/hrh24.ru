import styled from 'styled-components';

const TabContent = styled.div`
  box-sizing: border-box;
  margin-top: 15px;
  padding: 0 8px;
`;

export default TabContent;
