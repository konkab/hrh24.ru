import { createSelector } from 'reselect';

const selectGlobal = state => state.get('testRegistration');

const makeSelectPhone = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('phone')
  );

const makeSelectCode = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('code')
  );

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('loading')
  );

const makeSelectSmsWasSend = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('smsWasSend')
  );

const makeSelectVerified = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('verified')
  );

const makeSelectInvitationCode = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('invitationCode')
  );

const makeSelectUser = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.get('user')
  );

export {
  makeSelectPhone,
  makeSelectCode,
  makeSelectLoading,
  makeSelectSmsWasSend,
  makeSelectVerified,
  makeSelectInvitationCode,
  makeSelectUser
};
