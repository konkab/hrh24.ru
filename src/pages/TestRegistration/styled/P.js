import styled from 'styled-components';

const P = styled.p`
  color: ${props => (props.color ? props.color : 'inherit')};
  margin-top: ${props => (props.marginTop ? props.marginTop : 'inherit')};
`;

export default P;
