import { fromJS } from 'immutable';

import {
  CHANGE_LOGIN,
  CHANGE_PASSWORD,
  CHANGE_FLAGS,
  LOGIN_REQUEST_SUCCESS,
  LOGIN_REQUEST_FAIL
} from './constants';

const initialState = fromJS({
  error: '',
  flags: [],
  key: false,
  login: '',
  password: ''
});

function loginReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_LOGIN:
      return state.set('login', action.login);
    case CHANGE_PASSWORD:
      return state.set('password', action.password);
    case CHANGE_FLAGS:
      return state.set('flags', fromJS(action.flags));
    case LOGIN_REQUEST_SUCCESS:
      return state.set('key', action.key);
    case LOGIN_REQUEST_FAIL:
      return state.set('flags', fromJS([1])).set('error', action.errorMessage);
    default:
      return state;
  }
}

export default loginReducer;
