export const CHANGE_LOGIN = 'hh/pages/individualindividualLogin/CHANGE_LOGIN';
export const CHANGE_PASSWORD = 'hh/pages/individualLogin/CHANGE_PASSWORD';
export const CHANGE_FLAGS = 'hh/pages/individualLogin/CHANGE_FLAGS';

export const LOGIN_REQUEST = 'hh/pages/individualLogin/LOGIN_REQUEST';
export const LOGIN_REQUEST_SUCCESS = 'hh/pages/individualLogin/LOGIN_REQUEST_SUCCESS';
export const LOGIN_REQUEST_FAIL = 'hh/pages/individualLogin/LOGIN_REQUEST_FAIL';

export const LOGOUT = 'hh/pages/individualLogin/LOGOUT';
