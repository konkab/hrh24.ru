import styled from 'styled-components';

const Title = styled.h3`
  @media (max-width: 1020px) {
    text-align: center;
  }
`;

export default Title;
