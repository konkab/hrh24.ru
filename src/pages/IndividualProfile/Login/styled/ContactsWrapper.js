import styled from 'styled-components';

const ContactsWrapper = styled.div`
  display: flex;
  justify-content: flex-end
`;

export default ContactsWrapper;
