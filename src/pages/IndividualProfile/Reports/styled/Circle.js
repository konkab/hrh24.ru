import styled from 'styled-components';

const Circle = styled.div`
  background-color: ${props => props.backgroundColor};
  border-radius: 50%;
  height: 20px;
  margin-right: 10px;
  width: 20px;
  -webkit-print-color-adjust: exact;
`;

export default Circle;
