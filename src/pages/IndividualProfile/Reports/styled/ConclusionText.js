import styled from 'styled-components';

const ConclusionText = styled.div`
  font-size: 16px;
  color: #ffffff;
  letter-spacing: 0;
  line-height: 20px;
  margin-top: 20px;
`;

export default ConclusionText;
