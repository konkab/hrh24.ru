import styled from 'styled-components';

const ConclusionTitle = styled.div`
  font-size: 24px;
  font-weight: 600;
  color: #ffffff;
  letter-spacing: 0.33px;
  line-height: 28px;
`;

export default ConclusionTitle;
