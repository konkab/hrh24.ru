import styled from 'styled-components';

const BuyBlock = styled.div`
  background: #f0f2f5;
  margin: 30px 0;
  padding: 30px 35px;
`;

export default BuyBlock;
