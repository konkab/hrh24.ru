import styled from 'styled-components';

const LegendText = styled.div`
  font-size: 16px;
  color: #172b4d;
  letter-spacing: 0;
  line-height: 20px;
  margin-top: 20px;
  margin-bottom: 16px;
`;

export default LegendText;
