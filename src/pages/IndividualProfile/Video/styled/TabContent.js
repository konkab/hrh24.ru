import styled from 'styled-components';

const TabContent = styled.div`
  width: 100%;
`;

export default TabContent;
