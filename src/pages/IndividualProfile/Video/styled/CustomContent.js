import React from 'react';

const CustomContent = ({ data, elementProps }) => <div {...elementProps}>{data.content}</div>;

export default CustomContent;
