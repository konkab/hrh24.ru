import styled from 'styled-components';

const Container = styled.div`
  max-width: 640px;
  margin: 0 auto 20px;
  padding: 0 2rem;
`;

export default Container;
