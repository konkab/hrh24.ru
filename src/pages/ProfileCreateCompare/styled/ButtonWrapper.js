import styled from 'styled-components';

const ButtonWrapper = styled.div`
  margin-top: 39px;
  margin-bottom: 30px;
`;

export default ButtonWrapper;
