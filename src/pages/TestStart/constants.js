export const CHANGE_PHONE = 'hh/pages/test/start/CHANGE_PHONE';
export const CHANGE_CODE = 'hh/pages/test/start/CHANGE_CODE';

export const SEND_CREATE_REQUEST = 'hh/pages/test/start/SEND_CREATE_REQUEST';
export const SEND_CREATE_REQUEST_SUCCESS = 'hh/pages/test/start/SEND_CREATE_REQUEST_SUCCESS';
export const SEND_CREATE_REQUEST_FAIL = 'hh/pages/test/start/SEND_CREATE_REQUEST_FAIL';

export const SEND_VERIFY_REQUEST = 'hh/pages/test/start/SEND_VERIFY_REQUEST';
export const SEND_VERIFY_REQUEST_SUCCESS = 'hh/pages/test/start/SEND_VERIFY_REQUEST_SUCCESS';
export const SEND_VERIFY_REQUEST_FAIL = 'hh/pages/test/start/SEND_VERIFY_REQUEST_FAIL';

export const CHANGE_NAME = 'hh/pages/test/start/CHANGE_NAME';
export const CHANGE_SURNAME = 'hh/pages/test/start/CHANGE_SURNAME';
export const CHANGE_EMAIL = 'hh/pages/test/start/CHANGE_EMAIL';

export const USER_READ_REQUEST = 'hh/pages/test/start/USER_READ_REQUEST';
export const USER_READ_REQUEST_SUCCESS = 'hh/pages/test/start/USER_READ_REQUEST_SUCCESS';
export const USER_READ_REQUEST_FAIL = 'hh/pages/test/start/USER_READ_REQUEST_FAIL';

export const USER_UPDATE_REQUEST = 'hh/pages/test/start/USER_UPDATE_REQUEST';
export const USER_UPDATE_REQUEST_SUCCESS = 'hh/pages/test/start/USER_UPDATE_REQUEST_SUCCESS';
export const USER_UPDATE_REQUEST_FAIL = 'hh/pages/test/start/USER_UPDATE_REQUEST_FAIL';

export const CHECK_INVITE_CODE_REQUEST = 'hh/pages/test/start/CHECK_INVITE_CODE_REQUEST';
export const CHECK_INVITE_CODE_REQUEST_SUCCESS =
  'hh/pages/test/start/CHECK_INVITE_CODE_REQUEST_SUCCESS';
export const CHECK_INVITE_CODE_REQUEST_FAIL = 'hh/pages/test/start/CHECK_INVITE_CODE_REQUEST_FAIL';

export const CHECK_ORDER_REQUEST = 'hh/pages/test/start/CHECK_ORDER_REQUEST';
export const CHECK_ORDER_REQUEST_SUCCESS = 'hh/pages/test/start/CHECK_ORDER_REQUEST_SUCCESS';
export const CHECK_ORDER_REQUEST_FAIL = 'hh/pages/test/start/CHECK_ORDER_REQUEST_FAIL';
