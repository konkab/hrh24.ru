import styled from 'styled-components';

const ButtonField = styled.div`
  align-items: center;
  display: flex;
  width: ${props => (props.width ? props.width : 'calc(50% - 10px)')};
  margin-top: 40px;

  @media (max-width: 1020px) {
    flex-direction: column;
    margin: 20px auto;
    text-align: center;
    width: 100%;
  }
`;

export default ButtonField;
