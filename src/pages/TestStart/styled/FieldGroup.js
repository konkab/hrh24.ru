import styled from 'styled-components';

const FieldGroup = styled.div`
  display: ${props => (props.displayBlock ? 'block' : 'flex')};
  justify-content: space-between;
`;

export default FieldGroup;
