import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import reducer from 'pages/PartnerProfile/Partners/reducer';
import saga from 'pages/PartnerProfile/Partners/saga';
import {
  changeClientsPage,
  getClientsList,
  getPartner
} from 'pages/PartnerProfile/Partners/actions';
import {
  makeSelectClientsCount,
  makeSelectClientsList,
  makeSelectClientsLoading,
  makeSelectClientsPage,
  makeSelectPartnerRead,
  makeSelectPartnerLoading
} from 'pages/PartnerProfile/Partners/selectors';

import { BreadcrumbsItem, BreadcrumbsStateless } from '@atlaskit/breadcrumbs';
import Page, { Grid, GridColumn } from '@atlaskit/page';
import PageHeader from '@atlaskit/page-header';
import Pagination from '@atlaskit/pagination';
import Empty from 'components/Empty';
import Spinner from 'components/Spinner';

import ClientsTable from './Table';

class Clients extends Component {
  componentDidMount() {
    const id = parseInt(this.props.match.params.id, 10);
    const {
      props: { getClientsList, getPartner, partner }
    } = this;

    if (partner.id !== id) {
      getPartner({ id });
    }

    getClientsList({ id });
  }

  onBreadcrumbsClick = (event, pathname) => {
    event.preventDefault();
    this.props.history.push(pathname);
  };

  renderBreadcrumbs = () => {
    const {
      onBreadcrumbsClick,
      props: { partner }
    } = this;

    return (
      <BreadcrumbsStateless onExpand={() => {}}>
        <BreadcrumbsItem
          href="/partner/partners"
          onClick={event => onBreadcrumbsClick(event, '/partner/partners')}
          text="Партнеры"
          key="partners"
        />
        <BreadcrumbsItem
          href={`/partner/partners/${partner.id}`}
          onClick={event => onBreadcrumbsClick(event, `/partner/partners/${partner.id}`)}
          text={partner.full_name}
          key="partnerFull_name"
        />
        <BreadcrumbsItem
          href={`/partner/partners/${partner.id}/clients`}
          onClick={event => onBreadcrumbsClick(event, `/partner/partners/${partner.id}/clients`)}
          text="Клиенты"
          key="partnerClients"
        />
      </BreadcrumbsStateless>
    );
  };

  renderContent = () => {
    const {
      props: { changeClientsPage, count, list, loading, page, partner, tableLoading },
      renderBreadcrumbs
    } = this;

    let content = (
      <Grid layout="fluid">
        <GridColumn medium={12}>
          <Empty header="У партнера еще нет клиентов" />
        </GridColumn>
      </Grid>
    );

    if (count >= 1) {
      content = (
        <Grid layout="fluid">
          <GridColumn medium={12}>
            <ClientsTable key="partnersClientsTable" data={list} isLoading={tableLoading} />
            <Pagination
              key="pagination"
              value={page}
              total={Math.ceil(count / 25)}
              onChange={page => changeClientsPage({ page, id: partner.id })}
            />
          </GridColumn>
        </Grid>
      );
    } else if (loading) {
      content = (
        <Grid layout="fluid">
          <GridColumn medium={12}>
            <Spinner height="80vh" />
          </GridColumn>
        </Grid>
      );
    }

    return (
      <Grid layout="fluid">
        <GridColumn medium={12}>
          <PageHeader breadcrumbs={renderBreadcrumbs()}>Клиенты</PageHeader>
        </GridColumn>
        {content}
      </Grid>
    );
  };

  render() {
    const {
      props: { partner },
      renderContent
    } = this;

    return (
      <Page>
        <Helmet>
          <title>{partner.full_name}</title>
        </Helmet>
        {renderContent()}
      </Page>
    );
  }
}

Clients.propTypes = {
  changeClientsPage: PropTypes.func.isRequired,
  count: PropTypes.number.isRequired,
  getClientsList: PropTypes.func.isRequired,
  getPartner: PropTypes.func.isRequired,
  list: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  partner: PropTypes.object.isRequired,
  tableLoading: PropTypes.bool.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    changeClientsPage: value => dispatch(changeClientsPage(value)),
    getClientsList: value => dispatch(getClientsList(value)),
    getPartner: value => dispatch(getPartner(value))
  };
}

const mapStateToProps = createStructuredSelector({
  count: makeSelectClientsCount(),
  list: makeSelectClientsList(),
  loading: makeSelectPartnerLoading(),
  page: makeSelectClientsPage(),
  partner: makeSelectPartnerRead(),
  tableLoading: makeSelectClientsLoading()
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: 'partnerPartners', reducer });
const withSaga = injectSaga({ key: 'partnerPartners', saga });

export default compose(
  withRouter,
  withReducer,
  withSaga,
  withConnect
)(Clients);
