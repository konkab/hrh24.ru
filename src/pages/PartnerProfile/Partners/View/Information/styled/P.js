import styled from 'styled-components';

const P = styled.p`
  margin-top: ${props => (props.marginTop ? props.marginTop : 'inherit')};
`;

export default P;
