import React, { Component } from 'react';
import uniqid from 'uniqid';

import { DynamicTableStateless as DynamicTable } from '@atlaskit/dynamic-table';

import NameWrapper from './styled/NameWrapper';

class RequisitesTable extends Component {
  generateHead = () => ({
    cells: [
      {
        key: 'name',
        content: 'Название организации',
        isSortable: false,
        width: 25
      },
      {
        key: 'inn',
        content: 'ИНН',
        shouldTruncate: true,
        isSortable: false,
        width: 25
      },
      {
        key: 'kpp',
        content: 'КПП',
        shouldTruncate: true,
        isSortable: false,
        width: 25
      },
      {
        key: 'ogrn',
        content: 'ОГРН',
        shouldTruncate: true,
        width: 25
      }
    ]
  });

  generateRows = data =>
    data.map((item, index) => ({
      key: `row-${index}-${item.id}`,
      cells: [
        {
          key: uniqid(),
          content: <NameWrapper>{item.name}</NameWrapper>
        },
        {
          key: uniqid(),
          content: item.inn
        },
        {
          key: uniqid(),
          content: item.kpp
        },
        {
          key: uniqid(),
          content: item.ogrn
        }
      ]
    }));

  render() {
    const { data, isLoading } = this.props;

    return (
      <DynamicTable
        head={this.generateHead()}
        isLoading={isLoading}
        loadingSpinnerSize="large"
        onSetPage={() => {}}
        onSort={() => {}}
        rows={this.generateRows(data)}
      />
    );
  }
}

export default RequisitesTable;
