import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import isEmpty from 'lodash/isEmpty';
import { isEmail, isInn, isPhoneInProfile } from 'utils/validators';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

import { BreadcrumbsItem, BreadcrumbsStateless } from '@atlaskit/breadcrumbs';
import Button, { ButtonGroup } from '@atlaskit/button';
import Form from '@atlaskit/form';
import { Grid, GridColumn } from '@atlaskit/page';
import PageHeader from '@atlaskit/page-header';
import RouterLink from 'components/RouterLink';
import TextField from 'components/TextField';

import reducer from 'pages/PartnerProfile/Clients/reducer';
import saga from 'pages/PartnerProfile/Clients/saga';

import {
  changeClientRegistrationField,
  registerNewClient,
  resetClientRegistrationForm
} from 'pages/PartnerProfile/Clients/actions';
import {
  makeSelectRegistrationForm,
  makeSelectRegistrationLoading
} from 'pages/PartnerProfile/Clients/selectors';

import ButtonWrapper from './styled/ButtonWrapper';
import Container from './styled/Container';
import Field from './styled/Field';
import FieldGroup from './styled/FieldGroup';

class Registration extends Component {
  state = {
    formValid: {
      company_name: true,
      company_inn: true,
      first_name: true,
      email: true,
      phone_number: true,
      city: true
    }
  };

  componentDidMount() {
    const { resetClientRegistrationForm } = this.props;

    resetClientRegistrationForm();
  }

  onBreadcrumbsClick = (event, pathname) => {
    event.preventDefault();
    this.props.history.push(pathname);
  };

  onChangeFormField = event => {
    const {
      props: { changeClientRegistrationField },
      validateForm
    } = this;

    const field = {
      field: event.target.id,
      text: event.target.value
    };

    changeClientRegistrationField(field);
    validateForm(field);
  };

  onSubmitForm = event => {
    event.preventDefault();

    const {
      props: { registerNewClient, registrationForm },
      state: { formValid },
      validateForm
    } = this;
    const formValidationResult = {};
    let isValid = true;

    Object.entries(formValid).forEach(([key]) => {
      const field = {
        field: key,
        text: registrationForm[key]
      };
      formValidationResult[key] = validateForm(field, false);

      if (!formValidationResult[key]) {
        isValid = false;
      }
    });

    this.setState({
      formValid: formValidationResult
    });

    if (isValid) {
      registerNewClient();
    }
  };

  validateForm = ({ field, text }, setState = true) => {
    const { formValid } = this.state;
    let result = null;

    switch (field) {
      case 'company_name':
        result = !isEmpty(text);
        if (setState) {
          this.setState({
            formValid: {
              ...formValid,
              company_name: result
            }
          });
        }

        return result;
      case 'company_inn':
        result = isEmpty(text) || isInn(text);
        if (setState) {
          this.setState({
            formValid: {
              ...formValid,
              company_inn: result
            }
          });
        }

        return result;
      case 'first_name':
        result = !isEmpty(text);
        if (setState) {
          this.setState({
            formValid: {
              ...formValid,
              first_name: result
            }
          });
        }

        return result;
      case 'email':
        result = isEmail(text);
        if (setState) {
          this.setState({
            formValid: {
              ...formValid,
              email: result
            }
          });
        }

        return result;
      case 'phone_number':
        result = isPhoneInProfile(text);
        if (setState) {
          this.setState({
            formValid: {
              ...formValid,
              phone_number: result
            }
          });
        }

        return result;
      case 'city':
        result = !isEmpty(text);
        if (setState) {
          this.setState({
            formValid: {
              ...formValid,
              city: result
            }
          });
        }

        return result;
      default:
        return result;
    }
  };

  renderActions = () => (
    <ButtonGroup>
      <Button href="/partner/clients" component={RouterLink}>
        Отмена
      </Button>
    </ButtonGroup>
  );

  renderBreadcrumbs = () => (
    <BreadcrumbsStateless onExpand={() => {}}>
      <BreadcrumbsItem
        href="/partner/clients"
        onClick={event => this.onBreadcrumbsClick(event, '/partner/clients')}
        text="Клиенты"
        key="partnerClients"
      />
      <BreadcrumbsItem
        href="/partner/clients/create"
        onClick={event => this.onBreadcrumbsClick(event, '/partner/clients/create')}
        text="Регистрация"
        key="partnerClientsCreate"
      />
    </BreadcrumbsStateless>
  );

  renderForm = () => {
    const {
      onChangeFormField,
      onSubmitForm,
      props: { loading, registrationForm },
      state: { formValid }
    } = this;

    return (
      <Form>
        <FieldGroup>
          <Field>
            <TextField
              isInvalid={!formValid.company_name}
              required
              label="Название компании"
              name="company"
              id="company_name"
              onBlur={onChangeFormField}
              onChange={onChangeFormField}
              placeholder=""
              shouldFitContainer
              value={registrationForm.company_name}
            />
          </Field>
          <Field>
            <TextField
              isInvalid={!formValid.company_inn}
              label="ИНН компании"
              mask="999999999999"
              maskChar=""
              name="inn"
              id="company_inn"
              onBlur={onChangeFormField}
              onChange={onChangeFormField}
              placeholder=""
              shouldFitContainer
              value={registrationForm.company_inn}
            />
          </Field>
        </FieldGroup>
        <FieldGroup>
          <Field>
            <TextField
              isInvalid={!formValid.first_name}
              required
              label="Имя"
              name="name"
              id="first_name"
              onBlur={onChangeFormField}
              onChange={onChangeFormField}
              placeholder=""
              shouldFitContainer
              value={registrationForm.first_name}
            />
          </Field>
          <Field>
            <TextField
              label="Фамилия"
              name="last_name"
              id="last_name"
              onBlur={onChangeFormField}
              onChange={onChangeFormField}
              placeholder=""
              shouldFitContainer
              value={registrationForm.last_name}
            />
          </Field>
        </FieldGroup>
        <FieldGroup>
          <Field>
            <TextField
              isInvalid={!formValid.email}
              required
              label="Email"
              name="name"
              id="email"
              onBlur={onChangeFormField}
              onChange={onChangeFormField}
              placeholder=""
              shouldFitContainer
              value={registrationForm.email}
            />
          </Field>
          <Field>
            <TextField
              isInvalid={!formValid.phone_number}
              required
              mask="+79999999999"
              maskChar=""
              label="Телефон"
              name="phone"
              id="phone_number"
              onBlur={onChangeFormField}
              onChange={onChangeFormField}
              placeholder=""
              shouldFitContainer
              value={registrationForm.phone_number}
            />
          </Field>
        </FieldGroup>
        <FieldGroup>
          <Field>
            <TextField
              isInvalid={!formValid.city}
              required
              label="Город"
              name="city"
              id="city"
              onBlur={onChangeFormField}
              onChange={onChangeFormField}
              placeholder=""
              shouldFitContainer
              value={registrationForm.city}
            />
          </Field>
        </FieldGroup>
        <FieldGroup>
          <ButtonWrapper>
            <Button appearance="primary" isLoading={loading} onClick={onSubmitForm}>
              Зарегистрировать
            </Button>
          </ButtonWrapper>
        </FieldGroup>
      </Form>
    );
  };

  render() {
    return (
      <Container>
        <Helmet>
          <title>Регистрация клиента</title>
        </Helmet>
        <Grid layout="fluid">
          <GridColumn medium={12}>
            <PageHeader actions={this.renderActions()} breadcrumbs={this.renderBreadcrumbs()}>
              Регистрация клиента
            </PageHeader>
          </GridColumn>
          <GridColumn medium={12}>{this.renderForm()}</GridColumn>
        </Grid>
      </Container>
    );
  }
}

Registration.propTypes = {
  changeClientRegistrationField: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  registerNewClient: PropTypes.func.isRequired,
  registrationForm: PropTypes.object.isRequired,
  resetClientRegistrationForm: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    changeClientRegistrationField: value => dispatch(changeClientRegistrationField(value)),
    registerNewClient: () => dispatch(registerNewClient()),
    resetClientRegistrationForm: () => dispatch(resetClientRegistrationForm())
  };
}

const mapStateToProps = createStructuredSelector({
  loading: makeSelectRegistrationLoading(),
  registrationForm: makeSelectRegistrationForm()
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: 'partnerClients', reducer });
const withSaga = injectSaga({ key: 'partnerClients', saga });

export default compose(
  withRouter,
  withReducer,
  withSaga,
  withConnect
)(Registration);
