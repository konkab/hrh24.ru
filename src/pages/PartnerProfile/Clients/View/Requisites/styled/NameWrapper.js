import styled from 'styled-components';

const NameWrapper = styled.span`
  display: flex;
  align-items: center;
  height: 40px;
`;

export default NameWrapper;
