import React, { Component } from 'react';
import uniqid from 'uniqid';
import { Link } from 'react-router-dom';
import * as moment from 'moment';
import 'moment/locale/ru';

import { DynamicTableStateless as DynamicTable } from '@atlaskit/dynamic-table';
import Rouble from 'components/Rouble';

import NameWrapper from './styled/NameWrapper';

class FinancesTable extends Component {
  generateHead = () => ({
    cells: [
      {
        key: 'source',
        content: 'Источник',
        isSortable: false,
        width: 20
      },
      {
        key: 'sourceType',
        content: 'Тип источника',
        shouldTruncate: true,
        isSortable: false,
        width: 15
      },
      {
        key: 'name',
        content: 'Операция',
        shouldTruncate: true,
        isSortable: false,
        width: 20
      },
      {
        key: 'created_at',
        content: 'Дата операции',
        shouldTruncate: true,
        width: 15
      },
      {
        key: 'original_amount',
        content: 'Сумма',
        shouldTruncate: true,
        width: 15
      },
      {
        key: 'reward_amount',
        content: 'Ваше вознаграждение',
        shouldTruncate: true,
        width: 15
      }
    ]
  });

  generateRows = data =>
    data.map((item, index) => ({
      key: `row-${index}-${item.id}`,
      cells: [
        {
          key: uniqid(),
          content: (
            <NameWrapper>
              {item.source_type === 'company' ? (
                <Link to={`/partner/clients/${item.source.id}`}>{item.source.name}</Link>
              ) : (
                ''
              )}
              {item.source_type === 'partner' ? (
                <Link to={`/partner/partners/${item.source.id}`}>{item.source.name}</Link>
              ) : (
                ''
              )}
            </NameWrapper>
          )
        },
        {
          key: uniqid(),
          content: item.source_type === 'company' ? 'Компания' : 'Партнер'
        },
        {
          key: uniqid(),
          content: item.name
        },
        {
          key: uniqid(),
          content: moment(item.created_at).format('DD.MM.YYYY')
        },
        {
          key: uniqid(),
          content: (
            <NameWrapper>
              {item.original_amount.toLocaleString()}&nbsp;<Rouble>₽</Rouble>
            </NameWrapper>
          )
        },
        {
          key: uniqid(),
          content: (
            <NameWrapper>
              {item.reward_amount.toLocaleString()}&nbsp;<Rouble>₽</Rouble>
            </NameWrapper>
          )
        }
      ]
    }));

  render() {
    const { data, isLoading, rewardAmountSum } = this.props;

    return [
      <div key="title" style={{ marginBottom: 15 }}>
        <h3>
          Полученое вознаграждение за выбранный период: {rewardAmountSum.toLocaleString()}{' '}
          <Rouble>₽</Rouble>
        </h3>
      </div>,
      <DynamicTable
        key="table"
        head={this.generateHead()}
        isLoading={isLoading}
        loadingSpinnerSize="large"
        onSetPage={() => {}}
        onSort={() => {}}
        rows={this.generateRows(data)}
      />
    ];
  }
}

export default FinancesTable;
