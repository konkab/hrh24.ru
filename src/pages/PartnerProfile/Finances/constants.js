export const GET_FINANCES_LIST = 'hh/pages/partner/finances/GET_FINANCES_LIST';
export const GET_FINANCES_LIST_SUCCESS = 'hh/pages/partner/finances/GET_FINANCES_LIST_SUCCESS';
export const GET_FINANCES_LIST_FAIL = 'hh/pages/partner/finances/GET_FINANCES_LIST_FAIL';
export const CHANGE_FINANCES_PAGE = 'hh/pages/partner/finances/CHANGE_FINANCES_PAGE';
export const CHANGE_DATE_FILTER = 'hh/pages/partner/finances/CHANGE_DATE_FILTER';
export const CHANGE_TYPE_FILTER = 'hh/pages/partner/finances/CHANGE_TYPE_FILTER';
