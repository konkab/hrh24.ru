import styled from 'styled-components';

const MainBlock = styled.div`
  margin-top: 32px;
  padding-bottom: 27px;
  border-bottom: 2px solid rgba(9, 30, 66, 0.13);
`;

export default MainBlock;
