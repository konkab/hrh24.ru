import styled from 'styled-components';

const TableLine = styled.div`
  margin: 15px 0;
  position: relative;
`;

export default TableLine;
