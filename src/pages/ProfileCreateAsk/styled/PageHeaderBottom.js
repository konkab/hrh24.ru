import styled from 'styled-components';

const PageHeaderBottom = styled.div`
  margin-top: -18px;
`;

export default PageHeaderBottom;
