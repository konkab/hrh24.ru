import styled from 'styled-components';

const ImageWrapper = styled.div`
  ${props =>
    props.flex
      ? `
    align-items: center;
    display: flex;
    justify-content: flex-start;
  `
      : ''};
  margin: ${props => (props.margin ? props.margin : 'inherit')};
`;

export default ImageWrapper;
