import styled from 'styled-components';

const P = styled.p`
  line-height: 20px;
  margin-top: ${props => (props.marginTop ? props.marginTop : 'inherit')};
`;

export default P;
