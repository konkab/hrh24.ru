export const DOCUMENTS_LIST_REQUEST = 'hh/pages/documents/DOCUMENTS_LIST_REQUEST';
export const DOCUMENTS_LIST_SUCCESS = 'hh/pages/documents/DOCUMENTS_LIST_SUCCESS';
export const DOCUMENTS_LIST_FAIL = 'hh/pages/documents/DOCUMENTS_LIST_FAIL';

export const DOCUMENT_READ_REQUEST = 'hh/pages/documents/DOCUMENT_READ_REQUEST';
export const DOCUMENT_READ_SUCCESS = 'hh/pages/documents/DOCUMENT_READ_SUCCESS';
export const DOCUMENT_READ_FAIL = 'hh/pages/documents/DOCUMENT_READ_FAIL';
